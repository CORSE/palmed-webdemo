# palmed-webdemo

Demo website for Palmed. Intended to be publicly available.

## Setup

You should clone this repository with its submodules, ie.

```bash
git clone --recurse-submodules git@gitlab.inria.fr:CORSE/palmed-webdemo.git
```

or, if you already cloned it,
```bash
git submodule init
git submodule update
```

Then, set up a virtual environment and configure the website:

```bash
# Create the virtualenv and install dependencies
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt

# Create the configuration file
cp palmedweb/settings.XXX.py palmedweb/settings.py  # where XXX = dev or prod
$EDITOR palmedweb/settings.py  # and edit all `FIXME`s

# Migrate the database
./manage.py migrate
```

### Compiling dependencies

```bash
cd contrib/extract-xed-instruction-database
cmake .
make -j
```

## Running

### For development

```bash
./manage.py runserver
```

You might also want to create a superuser with `./manage.py createsuperuser`.

The settings in `settings.dev.py` are sensible defaults; a `sqlite3` database
is perfectly fine for dev purposes.

### For production

You will need to reverse-proxy the WSGI application, through eg. `nginx`, and
serve the WSGI application, eg. with `gunicorn`.
