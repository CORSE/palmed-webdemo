function demo_hide_response() {
    let hide_selectors = [
        '#res_error',
        '#res_unsupported_instrs',
        '#res_stats',
        '#res_resource_usage'
    ];
    let empty_selectors = [
        '#res_error_verbatim',
        '#res_unsupported_instrs_list',
        '#res_cycles',
        '#res_ipc',
        '#res_resource_usage'
    ];
    for(selector of hide_selectors) {
        $(selector).hide();
    }
    for(selector of empty_selectors) {
        $(selector).html("");
    }
}

function receive_demo_data(data) {
    const decimal_places = 2;

    let unsupported_instrs_frame = $('#res_unsupported_instrs');
    if(data['unsupported_instructions'].length != 0) {
        unsupported_list = $('#res_unsupported_instrs_list');
        let list_content = "";
        for(instr of data['unsupported_instructions'])
            list_content += `<li>${instr}</li>`
        unsupported_list.html(list_content);
        unsupported_instrs_frame.show();
    }

    $("#res_cycles").html(data['cycles'].toFixed(decimal_places));
    $("#res_ipc").html(data['IPC'].toFixed(decimal_places));

    let resources_stats = "";
    for(resource of data['resources']) {
        let bar_critical = "fine";
        if(resource['norm_weight'] > 0.99)
            bar_critical = "crit";
        else if(resource['norm_weight'] > 0.90)
            bar_critical = "warn";
        else if(resource['norm_weight'] > 0.65)
            bar_critical = "meh";

        let resource_details = "";
        for(instr_detail of resource['usage']) {
            resource_details += `
            <div class="instr_detail_label">${instr_detail['instr']}</div>
            <div class="instr_detail_usage">
              <div class="progressbar_label_parent">
                <div>
                  <div class="progressbar_label">${instr_detail['weight'].toFixed(decimal_places)}</div>
                </div>
              </div>
              <div
                class="progressbar_fill"
                style="width: ${Math.round(100 * instr_detail['norm_weight'])}%"
              ></div>
            </div>
            `;
        }
        resources_stats += `
        <div class="resource_collapse">
          <i class="fa fa-plus-square" aria-hidden="true"></i>
        </div>
        <div class="resource_name">${resource["name"]}</div>
        <div class="resource_usage">
            <div class="progressbar_label_parent">
                <div>
                    <div class="progressbar_label">${resource['weight'].toFixed(decimal_places)}</div>
                </div>
            </div>
            <div class="progressbar_fill resource_bar resource_bar_${bar_critical}" style="width: ${resource['weight_percent']}%;"></div>
        </div>
        <div class="resource_details" style="display: none;">
            ${resource_details}
        </div>
        `;
    }
    $("#res_resource_usage").html(resources_stats);

    $("#res_resource_usage").children(".resource_name, .resource_usage, .resource_collapse")
        .on("click", function(event) {
            let sibling = $(event.target)
                .closest('.resource_name, .resource_usage, .resource_collapse');
            let hideblock = sibling.nextAll('.resource_details').first();
            let plusblock = hideblock.prevAll('.resource_collapse').first();
            let wasHidden = hideblock.is(":hidden");
            hideblock.toggle();
            if(wasHidden)
                plusblock.html('<i class="fa fa-minus-square" aria-hidden="true"></i>');
            else
                plusblock.html('<i class="fa fa-plus-square" aria-hidden="true"></i>');
        });

    $("#res_stats").show();
    $("#res_resource_usage").show();
}

function update_demo(endpoint, mapping_slug, asm, csrf_token) {
    demo_hide_response();
    $.post(endpoint, data={
        mapping: mapping_slug,
        asm: asm,
        csrfmiddlewaretoken: csrf_token
    }, receive_demo_data, "json")
        .fail(function(response) {
            let resp_json = $.parseJSON(response.responseText);
            let html_msg = resp_json['msg'].replace(/(?:\r\n|\r|\n)/g, '<br>');
            $("#res_error").show();
            $("#res_error_verbatim").html(html_msg);
        });
}
