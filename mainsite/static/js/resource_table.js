var last_data = {};
var sorted_by = ["name", false];
var sort_headers = ["name", "repr", "IPC", "latency"];

function add_sort_click_handlers() {
  for(let header of sort_headers) {
    $(".sort_btn_" + header).click(function() { table_sort_by(header); });
  }
}

function fill_sort_header() {
  for(header of sort_headers) {
    chr = "—";
    if(header == sorted_by[0]) {
      if(sorted_by[1])
        chr = "🞁";
      else
        chr = "🞃";
    }
    $(".sort_btn_" + header).html(chr);
  }
}

function fill_table_with_json(data) {
  tbody = ""
  sorted_instructions = [...data['instructions']];
  if(sorted_by[0] != "name") {
    function comp(a, b) {
      if(a < b)
        return -1;
      else if(a == b)
        return 0;
      return 1;
    }
    let sort_key = sorted_by[0];
    sorted_instructions.sort(function(a, b) {
      comp_val = comp(a[sort_key], b[sort_key]);
      if(comp_val == 0)
        comp_val = comp(a['name'], b['name']);
      return comp_val;
    });
  }
  if(sorted_by[1]) {
    sorted_instructions.reverse();
  }

  for(instr of sorted_instructions) {
    mapping = "";
    cur_mapping = data['mapping'][instr['repr']];
    first = true;
    for(resource in cur_mapping) {
      if(!first)
        mapping += '<span class="resplus">+</span>';
      else
        first = false;
      let frac = "";
      if(cur_mapping[resource]['weight_den'] == 1)
        frac = `<span class="resnat">${cur_mapping[resource]['weight_num']}</span>`;
      else
        frac = '<span class="resnum">'
        + `${cur_mapping[resource]['weight_num']}`
        + '</span><span class="resfracbar">/</span><span class="resden">'
        + `${cur_mapping[resource]['weight_den']}`
        + '</span>';
      mapping += '<span class="reschunk">'
        + `<span class="resweight" title="${cur_mapping[resource]['weight']}">`
        + frac
        + "</span>"
        + `<span class="reslabel">`
        + `${resource}`
        + "</span></span>";
    }
    repr = "(self)";
    row_class = "";
    if(!instr['is_repr']) {
      repr = instr['repr'];
    } else {
      row_class += ".instr_self_repr"
    }
    tbody += `<tr>
 <td>${instr['name']}</td>
 <td>${repr}</td>
 <td>${instr['IPC']}</td>
 <td>${instr['latency']}</td>
 <td>${mapping}</td>
      </tr>`
  }

  if(!tbody)
    tbody = `<tr><td colspan="5">No instructions</td></tr>`;
  $('#mapping_table_body').html(tbody);
}

function refill_table(data) {
  fill_sort_header();
  fill_table_with_json(data);
}

function receive_data(data) {
  last_data = data;
  refill_table(data);
}

function update_table(endpoint, q) {
  $.getJSON(
    endpoint + encodeURIComponent(q),
    receive_data
  );
}

function table_sort_by(header) {
  if(sorted_by[0] == header)
    sorted_by[1] = !sorted_by[1];
  else
    sorted_by = [header, false];
  refill_table(last_data);
}

function delay_callback(fn, ms) {
  // Thanks https://stackoverflow.com/a/1909508
  let timer = 0
  return function(...args) {
    clearTimeout(timer)
    timer = setTimeout(fn.bind(this, ...args), ms || 0)
  }
}
