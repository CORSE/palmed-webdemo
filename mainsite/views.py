from collections import defaultdict
from django.views.generic.base import TemplateView
from django.utils.safestring import mark_safe
from django.conf import settings
from mapping import mappings


class HomeView(TemplateView):
    """Homepage with static content"""

    template_name = "mainsite/home.html"


class DemoView(TemplateView):
    """Palmed demo based on a mapping"""

    template_name = "mainsite/demo.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["mappings"] = list(mappings.mappings.keys())
        context["default_code"] = settings.DEMO_DEFAULT_CODE
        return context


class DemoManView(TemplateView):
    """Explain the `DemoView` view"""

    template_name = "mainsite/demo_man.html"


class MappingsDescr(TemplateView):
    """List and describes the available mappings"""

    SOURCE_STRS = {
        "palmed": mark_safe('<span class="palmed"></span>'),
        "dummy": "Dummy Source",
        "uops": mark_safe('<a href="https://uops.info/">uops.info</a>'),
    }
    template_name = "mainsite/mappings.html"

    @classmethod
    def _get_source_str(cls, source):
        return cls.SOURCE_STRS.get(source, source)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        mapping_sources = defaultdict(list)
        for name, mapping in mappings.mappings.items():
            arch_details = settings.MICROARCHITECTURES.get(mapping.details.arch, {})
            mapping_sources[mapping.details.source].append(
                {
                    "name": name,
                    "isa": mapping.details.isa,
                    "arch": mapping.details.arch,
                    "arch_title": arch_details.get("name", None),
                    "arch_href": arch_details.get("href", None),
                    "cpu": mapping.details.cpu,
                    "cpu_href": mapping.details.cpu_href,
                    "descr": mapping.details.descr,
                    "source": mapping.details.source,
                    "sourcestr": self._get_source_str(mapping.details.source),
                }
            )

        mapping_descrs = []
        try:
            palmed_mappings = mapping_sources.pop("palmed")
            mapping_descrs.append(
                (mark_safe('<span class="palmed"></span>'), palmed_mappings)
            )
        except KeyError:
            pass
        for source, mappings_of_src in mapping_sources.items():
            mapping_descrs.append((self._get_source_str(source), mappings_of_src))

        context["mappings"] = mapping_descrs
        return context


class ResourceTableView(TemplateView):
    """Resource tables -- map an instruction to a set of resources"""

    template_name = "mainsite/resource_table.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["mappings"] = list(mappings.mappings.keys())
        return context
