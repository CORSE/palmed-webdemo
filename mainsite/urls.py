from django.urls import path, include
from .views import HomeView, DemoView, DemoManView, ResourceTableView, MappingsDescr

app_name = "mainsite"

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("demo", DemoView.as_view(), name="demo"),
    path("demo_man", DemoManView.as_view(), name="demo_man"),
    path("tables", ResourceTableView.as_view(), name="tables"),
    path("mappings", MappingsDescr.as_view(), name="mappings"),
]
