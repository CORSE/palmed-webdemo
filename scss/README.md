# Stylesheet

This website's style is made using
[scss](https://en.wikipedia.org/wiki/Sass_(stylesheet_language)).
The Makefile present in this directory compiles the scss to plain CSS, and
stores it in the appropriate directory (`mainsite/static/css`).

## Requirements

`sassc` must be installed.

## Compiling

```bash
make
```
