""" Parse some ASM in GAS syntax. """

import tempfile
import subprocess
from pathlib import Path
from contextlib import contextmanager
from django.conf import settings
from .mappings import ISA

import logging

logger = logging.getLogger(__name__)

try:
    import pipedream.asm.ir as pipeasm
    from pipedream.disasm.capstone import utils as pipe_capstone
    from pipedream.disasm.capstone import disassembler as pipe_capstone_ds
except ImportError:
    logger.warning("Could not load pipedream; loading ARMv8a asm will fail.")


class AsmParser:
    isa: ISA

    class ParseError(Exception):
        """Raised when the given asm cannot be parsed"""

        def __init__(self, msg, user_msg=None):
            super().__init__()
            self.msg = msg
            self.user_msg = user_msg or msg

        def __str__(self):
            return self.msg

    def __init__(self, elf_file, isa: ISA):
        self.isa = isa
        with open(elf_file, "rb") as elf_handle:
            self.bytes = self._parse(elf_handle)

    def _parse(self, handle):
        try:
            self.elf_parser = ElfParser(handle)
            return self.elf_parser.text_bytes
        except ElfParser.BadElf as exn:
            raise self.ParseError("bad elf file", "Internal error") from exn

    def _parse_text(self, handle, offset, size):
        handle.seek(offset)
        text_bytes = handle.read(size)
        if len(text_bytes) != size:
            raise self.ParseError("Cannot read .text", "Internal error")
        return text_bytes

    def _to_pipedream_instructions_x86(self) -> list[str]:
        try:
            extract_xed = subprocess.run(
                [
                    settings.EXTRACT_XED_IDB,
                    "disasm",
                ],
                input=self.bytes,
                capture_output=True,
                check=True,
            )
        except subprocess.CalledProcessError as exn:
            raise self.ParseError(
                "cannot read pipedream instructions", "Internal error"
            ) from exn
        instrs = extract_xed.stdout.decode("utf-8").strip().split()
        return instrs

    def _to_pipedream_instructions_armv8a(self) -> list[str]:
        pipe_arch: pipeasm.Architecture = pipeasm.Architecture.for_name(self.isa.value)
        disas = pipe_capstone_ds.Disassembler(pipe_arch).get_binary_dis(
            self.bytes, skip=True
        )
        out = []
        for insn in disas:
            pipe_name = pipe_capstone.cs_insn_class_pipedream(insn.cs_insn)
            if not pipe_name:
                logger.warning(
                    "Cannot decode insn %s %s [%s]",
                    insn.cs_insn.mnemonic,
                    insn.cs_insn.op_str,
                    insn.cs_insn.bytes.hex(),
                )
                continue
            out.append(pipe_name)
        return out

    def to_pipedream_instructions(self) -> list[str]:
        """Returns a list of instruction names, in pipedream format"""
        if self.isa == ISA.X86:
            return self._to_pipedream_instructions_x86()
        elif self.isa == ISA.Armv8a:
            return self._to_pipedream_instructions_armv8a()
        else:
            raise self.ParseError(f"Unsupported ISA {self.isa}")

    @staticmethod
    def format_assembler_error(as_error):
        """formats an error of `as` to a displayable string"""
        lines = as_error.strip().split("\n")
        out = []
        if "Assembler messages:" in lines[0]:
            lines = lines[1:]
        for line in lines:
            line = line.replace("{standard input}:", "")
            line = line.strip()
            out.append(line)
        return "\n".join(out)

    @classmethod
    def _assemble_with(cls, asm_string: str, as_name: str, isa: ISA, obj_path: Path):
        try:
            subprocess.run(
                [as_name, "-", "-o", obj_path.as_posix()],
                input=(asm_string + "\n").encode("utf-8"),
                check=True,
                capture_output=True,
            )
        except subprocess.CalledProcessError as exn:
            raise cls.ParseError(
                f"cannot interpret as valid {isa}",
                cls.format_assembler_error(exn.stderr.decode("utf-8")),
            ) from exn

    @classmethod
    def _assemble_x86(cls, asm_string: str, out: Path):
        return cls._assemble_with(asm_string, "as", ISA.X86, out)

    @classmethod
    def _assemble_armv8a(cls, asm_string: str, out: Path):
        return cls._assemble_with(asm_string, "aarch64-linux-gnu-as", ISA.Armv8a, out)

    @classmethod
    def from_string(cls, asm_string, isa: ISA):
        with tempfile.TemporaryDirectory() as tmp_dir:
            obj_path = Path(tmp_dir) / "obj.o"
            if isa == ISA.X86:
                cls._assemble_x86(asm_string, obj_path)
            elif isa == ISA.Armv8a:
                cls._assemble_armv8a(asm_string, obj_path)
            else:
                raise cls.ParseError(f"Unsupported ISA {isa}")
            return cls(obj_path.as_posix(), isa)


class ElfParser:
    """Trivial parser for an ELF file"""

    class BadElf(Exception):
        """Bad ELF file"""

    class Section:
        def __init__(self, **kwargs):
            self.name = ""
            self.sh_name = None
            self.sh_type = None
            self.sh_offset = None
            self.sh_size = None
            self.__dict__.update(kwargs)

        def read_name(self, handle, shstr_offset):
            handle.seek(shstr_offset + self.sh_name)
            name = b""
            while True:
                cur_ch = handle.read(1)
                if not cur_ch or cur_ch == b"\x00":
                    break
                name += cur_ch
            self.name = name.decode("utf-8")

    def __init__(self, handle):
        self.handle = handle
        self.handle.seek(0)
        magic = self.handle.read(0x04)
        if magic != b"\x7FELF":
            raise self.BadElf("bad magic number")

        bits = int.from_bytes(self.handle.read(1), "little")
        if bits not in [1, 2]:
            raise self.BadElf("bad class (32 or 64 bits)")
        self.is_64 = bits == 2

        endianness = int.from_bytes(self.handle.read(1), "little")
        if endianness not in [1, 2]:
            raise self.BadElf("bad endianness")
        self.endianness = "little" if endianness == 1 else "big"

        # offset of the section header table
        self.handle.seek(0x28 if self.is_64 else 0x20)
        self.section_header_offset = self._read_int()

        # Number of sections
        self.handle.seek(0x3C if self.is_64 else 0x30)
        self.shnum = self._read_int(2)

        # Read section headers
        self.handle.seek(self.section_header_offset)
        self.sections = []
        shstr_offs = None
        for _ in range(self.shnum):
            cur_section = self._read_section()
            self.sections.append(cur_section)
            if cur_section.sh_type == 0x03:
                shstr_offs = cur_section.sh_offset
        if shstr_offs:
            for section in self.sections:
                section.read_name(self.handle, shstr_offs)
        else:
            raise self.BadElf("no .shstrtab")

        # Find .text
        self.text_bytes = None
        for section in self.sections:
            if section.name == ".text":
                self.text_bytes = self._read_section_bytes(section)
                break
        else:
            raise self.BadElf("no .text")

    def _read_section(self):
        section = self.Section()
        section.sh_name = self._read_int(4)
        section.sh_type = self._read_int(4)
        self.handle.read(16 if self.is_64 else 8)  # discard sh_flags, sh_addr
        section.sh_offset = self._read_int()
        section.sh_size = self._read_int()
        self.handle.read(
            2 * 4 + (16 if self.is_64 else 8)
        )  # discard until next section
        return section

    def _read_section_bytes(self, section):
        self.handle.seek(section.sh_offset)
        return self.handle.read(section.sh_size)

    def _read_int(self, size=None):
        """Read an int: 32 or 64 bits depending on size, or `size` bytes if set"""
        size_read = size or (8 if self.is_64 else 4)
        byt = self.handle.read(size_read)
        return int.from_bytes(byt, self.endianness)
