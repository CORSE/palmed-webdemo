""" Read, interpret and cache a PALMED mapping from a pickle file """

import pickle
import logging
import enum
from functools import total_ordering
from fractions import Fraction
from collections import defaultdict
import typing as ty

import palmed_model

from django.conf import settings

logger = logging.getLogger(__name__)


def _resource_sort_key(res_name):
    if res_name[0] == "R":
        res_suffix = res_name[1:]
        try:
            res_num = int(res_suffix)
        except ValueError:
            res_num = 1000
        return (res_num, res_suffix)
    return (1010, res_name)


class ISA(enum.Enum):
    X86 = "x86"
    Armv8a = "ARMv8a"


class Mapping:
    """A mapping read from PALMED"""

    mapping: palmed_model.PalmedModel
    slug: str
    path: str
    arch: str
    isa: ISA

    class BadMapping(Exception):
        def __init__(self, mapping, what):
            self._mapping = mapping
            self._what = what
            super().__init__()

        def __str__(self):
            return "Bad mapping file '{}': {}".format(self._mapping.path, self._what)

    class BadBasicBlock(Exception):
        """Raised when a basic block cannot be analyzed"""

    def __init__(self, slug, details):
        self.slug = slug
        self.details = details
        self.path = details.path
        self.arch = details.arch
        self.isa = ISA(details.isa)
        try:
            self.mapping = palmed_model.PalmedModel.from_path(details.path)
        except FileNotFoundError as exn:
            raise self.BadMapping(self, "file not found") from exn
        except PermissionError as exn:
            raise self.BadMapping(self, "permission error") from exn
        except palmed_model.BadModel as exn:
            raise self.BadMapping(self, f"Could not interpret model: {exn}") from exn

    def analyze_instructions(self, instructions):
        @total_ordering
        class ResourceAggregation:
            def __init__(self, weight=0.0, instrs=None):
                self.weight = weight
                self.instrs = instrs or []

            def add(self, instr, weight):
                self.weight += weight
                self.instrs.append((instr, weight))

            def instrs_analysis(self):
                max_instr_weight = max(map(lambda x: x[1], self.instrs))
                return [
                    {
                        "instr": instr,
                        "weight": i_weight,
                        "share": i_weight / self.weight,
                        "norm_weight": i_weight / max_instr_weight,
                    }
                    for (instr, i_weight) in self.instrs
                ]

            def __eq__(self, other):
                if not isinstance(other, ResourceAggregation):
                    return super().__eq__(other)
                return self.weight == other.weight

            def __lt__(self, other):
                if not isinstance(other, ResourceAggregation):
                    return super().__lt__(other)
                return self.weight < other.weight

        unsupported = set()
        eq_instrs = []
        for instr in instructions:
            try:
                eq_instrs.append(
                    (self.mapping.class_of(self.mapping.get_instr(instr)), instr)
                )
            except KeyError:
                unsupported.add(instr)

        if not eq_instrs:  # No supported instruction in this block
            raise self.BadBasicBlock(
                "No supported instruction. Unsupported: {}".format(
                    ", ".join(unsupported)
                )
            )

        resources = defaultdict(ResourceAggregation)
        for (instr, orig_instr) in eq_instrs:
            for resource, weight in self.mapping.resource_usage(instr).items():
                if weight < 1e-6:  # prune epsilon-used resources
                    continue
                resources[resource].add(orig_instr, weight)

        if not resources:  # No resources used
            raise self.BadBasicBlock("No resource used")

        max_weight = max(resources.values()).weight

        serializable_eq_instrs = [(eq.name, orig) for (eq, orig) in eq_instrs]

        out = {
            "resources": sorted(
                [
                    {
                        "name": self.mapping.resource_label.get(resource, resource),
                        "weight": res_weight.weight,
                        "norm_weight": res_weight.weight / max_weight,
                        "weight_percent": round((100 * res_weight.weight) / max_weight),
                        "usage": res_weight.instrs_analysis(),
                    }
                    for resource, res_weight in resources.items()
                ],
                key=lambda x: _resource_sort_key(x["name"]),
            ),
            "cycles": max_weight,
            "IPC": len(eq_instrs) / max_weight,
            "unsupported_instructions": list(unsupported),
            "equivalent_instructions": serializable_eq_instrs,
        }
        return out


mappings = {slug: Mapping(slug, details) for slug, details in settings.MAPPINGS.items()}
