from functools import wraps

from django.views.generic.base import View, TemplateView, ContextMixin
from django.http import HttpResponse, JsonResponse

from . import mappings
from . import parse_asm


class JsonView(ContextMixin, View):
    """Abstract class-based view returning a JSON response"""

    class ApiError(Exception):
        def __init__(self, status, msg, is_json=False):
            self.status = status
            self.msg = msg
            self.is_json = is_json
            super().__init__()

        def __str__(self):
            return self.msg

        def http_response(self):
            if self.is_json:
                return JsonResponse(self.msg, status=self.status)
            return HttpResponse(self.msg, status=self.status)

    http_method_names = ["get", "post"]

    def get_json(self, method, context):
        """Returns the answer to this query, as json-convertible term."""
        pass

    def get(self, req, *args, **kwargs):
        try:
            r_args = req.GET
            r_args.update(kwargs)
            context = self.get_context_data(**r_args)
            return JsonResponse(self.get_json("get", context))
        except self.ApiError as exn:
            return exn.http_response()

    def post(self, req, *args, **kwargs):
        try:
            r_args = req.POST
            r_args.update(kwargs)
            context = self.get_context_data(**r_args)
            return JsonResponse(self.get_json("post", context))
        except self.ApiError as exn:
            return exn.http_response()


class MappingBoundMixin:
    """A mixin for a `JsonView` that expects a `mapping` argument."""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            mapping_name = kwargs["mapping"][0]
        except (KeyError, IndexError) as exn:
            raise JsonView.ApiError(400, "Missing argument: mapping") from exn

        try:
            mapping = mappings.mappings[mapping_name]
        except KeyError as exn:
            raise JsonView.ApiError(
                400, "No such mapping: {}".format(mapping_name)
            ) from exn

        context["mapping"] = mapping
        return context


class ListMappingsView(JsonView):
    """List the supported mappings"""

    def get_json(self, method, context):
        return {
            name: {"arch": mapping.arch} for name, mapping in mappings.mappings.items()
        }


class FindInstructionBaseView(MappingBoundMixin, JsonView):
    """Find an instruction matching a query term"""

    allow_all = False  # Allow querying the whole mapping

    def get_instructions(self, context):
        """Get the list of matching instructions"""

        full_dataset = False

        try:
            query = context["q"][0]
        except (KeyError, IndexError) as exn:
            raise self.ApiError(400, "Missing argument: q") from exn
        query_terms = query.split()
        if not query_terms:
            if self.allow_all:
                full_dataset = True
            else:
                raise self.ApiError(400, "Bad argument: q must not be empty")
        elif not any(map(lambda x: len(x) > 2, query_terms)):
            if self.allow_all:
                full_dataset = True
            else:
                raise self.ApiError(
                    400, "Bad argument: q must contain at least two characters"
                )

        if full_dataset:
            return context["mapping"].ordered_instrs

        return list(
            filter(
                lambda x: all(map(lambda term: term in x, query_terms)),
                context["mapping"].ordered_instrs,
            )
        )


class FindInstructionView(FindInstructionBaseView):
    """Find an instruction matching a query term"""

    def get_json(self, method, context):
        return {"instructions": self.get_instructions(context)}


class InstructionTableView(FindInstructionBaseView):
    """Find instructions matching a query and return their mapping data"""

    allow_all = True

    def get_json(self, method, context):
        instructions = self.get_instructions(context)
        mapping = context["mapping"]
        instructions_data = []
        reprs_used = set()
        for instr_name in instructions:
            instr = mapping.supported_instrs[instr_name]
            instructions_data.append(
                {
                    "name": instr_name,
                    "IPC": instr.IPC_fmt,
                    "latency": instr.latency_fmt,
                    "repr": instr.repr,
                    "is_repr": instr_name == instr.repr,
                }
            )
            reprs_used.add(instr.repr)

        instr_mapping = {instr: mapping.pretty_mapping[instr] for instr in reprs_used}

        out = {
            "instructions": instructions_data,
            "mapping": instr_mapping,
            "labels": mapping.res_label,
        }
        return out


class MapAsmView(MappingBoundMixin, JsonView):
    """Takes some ASM code in argument, returns data computed from the mapping

    Parameters:
    * `mapping`: mapping slug
    * `asm`: the assembly code to analyze
    """

    http_method_names = ["post"]

    def get_json(self, method, context):
        mapping = context["mapping"]

        try:
            parser = parse_asm.AsmParser.from_string(context["asm"][0], mapping.isa)
            instrs = parser.to_pipedream_instructions()
        except parse_asm.AsmParser.ParseError as exn:
            resp = {
                "msg": exn.user_msg,
            }
            raise JsonView.ApiError(422, resp, is_json=True) from exn

        try:
            analysis = mapping.analyze_instructions(instrs)
        except mapping.BadBasicBlock as exn:
            resp = {"msg": str(exn)}
            raise JsonView.ApiError(422, resp, is_json=True) from exn
        analysis["instructions"] = instrs
        return analysis
