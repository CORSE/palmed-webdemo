from django.urls import path, include
from .views import (
    ListMappingsView,
    FindInstructionView,
    InstructionTableView,
    MapAsmView,
)

app_name = "mapping"

urlpatterns = [
    path("mappings", ListMappingsView.as_view(), name="list_mappings"),
    path("find_instruction", FindInstructionView.as_view(), name="find_instruction"),
    path(
        "table_find_instruction",
        InstructionTableView.as_view(),
        name="table_find_instruction",
    ),
    path("map_asm", MapAsmView.as_view(), name="map_asm"),
]
