from .settings_base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ""  # FIXME

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []  # FIXME

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {  # FIXME
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Palmed Mappings
# This must be of the form `"slug": MappingDescr(...)`, the mappings being in the
# format produced by Palmed
MAPPINGS = {}  # FIXME
