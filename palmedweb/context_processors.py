from django.conf import settings


def templates_additional_context(request):
    """ Makes `settings.TEMPLATES_ADDITIONAL_CONTEXT` available to every template """
    return settings.TEMPLATES_ADDITIONAL_CONTEXT
