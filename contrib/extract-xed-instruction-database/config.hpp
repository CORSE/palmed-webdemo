
/// common defines for all files in this tool

#pragma once

// #ifdef NDEBUG
// #undef NDEBUG
// #endif

/// enable/disable extra & stricter error checking
#ifndef XED_EXTRACT_DEVELOPER_MODE
// #  define XED_EXTRACT_DEVELOPER_MODE 1
#  define XED_EXTRACT_DEVELOPER_MODE 0
#endif
